import * as R from 'ramda'
import { existsSync, readFileSync } from 'fs'
import * as GM from 'gm'
const IM = GM.subClass({ imageMagick: true })

// TODO: change if sizes of cards and pages become parametrable
export const xRepeat = 3
export const yRepeat = 3
export const CARDS_PER_PAGES: number = xRepeat * yRepeat
export const CARD_WIDTH = 635 // 3 * -> 1905
export const CARD_HEIGHT = 880 // 3 * -> 2640
export const PAGE_WIDTH = 2100
export const PAGE_HEIGHT = 2970
export const LEFT_BORDER: number = (PAGE_WIDTH - 3 * CARD_WIDTH) / 2
export const BOTTOM_BORDER: number = (PAGE_HEIGHT - 3 * CARD_HEIGHT) / 2

const bufferIsJpeg: (b: Buffer) => boolean = (b) => !!b && b.length > 2 && b[0] === 255 && b[1] === 216 && b[2] === 255

const areBufferJpeg: (files: Buffer[]) => boolean = R.reduce(
  (acc: boolean, elem: Buffer) => acc && bufferIsJpeg(elem),
  true,
)

const make1page: (images: string[], dest: string, filename: string) => Promise<string> = async (images, dest, filename) => {
  const page: GM.State = IM(0, 0, '#fff').setFormat('jpeg') // .resize(PAGE_HEIGHT, PAGE_WIDTH)

  if (images.length > CARDS_PER_PAGES) {
    throw Error(`too many images (${images.length}) for one page`)
  }

  // images.map((n: string) => {
  //   IM(n).size((err, value) => {
  //     if (value.width !== CARD_WIDTH || value.height !== CARD_HEIGHT) {
  //       throw Error(`incorrect size for ${n}: ${value.width} / ${value.height} instead of ${CARD_WIDTH} / ${CARD_HEIGHT}`)
  //     }
  //   })
  // })

  images.map((v: string, i: number) => {
    const cardX: number = i % xRepeat
    const cardY: number = Math.floor(i / yRepeat)
    const x: number = cardX * CARD_WIDTH // + LEFT_BORDER
    const y: number = cardY * CARD_HEIGHT // + BOTTOM_BORDER

    page
      // .in('-page', `${PAGE_WIDTH}x${PAGE_WIDTH}+${x}+${y}`) // for bordered this is the way, border value is missing from x/y
      .in('-page', `${CARD_WIDTH}x${CARD_HEIGHT} +${x}+${y}`)
      .in(v)
  })

  page.mosaic()

  // TODO: change ?
  const name = `${dest}${filename}.jpg` // TODO: changer ça pour nom en argument + numéro de page
  page  // .charcoal(.5) -> for low cost test print
    .write(name, () => ({}))

  return name
}

/**
 * Group all the images in pages for print
 *
 * @param files string[] a list of images uri to be grouped
 * @returns a Promise with the uri of jpg created
 */
export const files2print: (images: string[], dest: string, filename: string) => Promise<string[]> = async (images, dest, filename) => {
  images.map((f) => {
    if (f.slice(-4) !== '.jpg') throw Error(`${f} does not have a .jpg extension`)
    if (!existsSync(f)) throw Error(`${f} does not exists`)
  })

  const files: Buffer[] = R.map(readFileSync)(images) as Buffer[]
  if (!areBufferJpeg(files)) throw Error('A buffer is not a image/jpeg')

  // todo: check if files have the right size ?

  // TODO: check dest exists

  const cardsCount: number = images.length
  const pagesCount: number = Math.ceil(cardsCount / CARDS_PER_PAGES)
  const digits = pagesCount.toString().length

  const promises = [...Array(pagesCount)].map((x, page) => {
    const first: number = Math.max(0, CARDS_PER_PAGES * page)
    const last: number = Math.min(images.length, CARDS_PER_PAGES * (page + 1))
    return make1page(images.slice(first, last), dest, `${filename}${(page + 1).toString().padStart(digits, '0')}`)
  })

  return Promise.all(promises)
}

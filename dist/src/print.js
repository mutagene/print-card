"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const R = __importStar(require("ramda"));
const fs_1 = require("fs");
const GM = __importStar(require("gm"));
const IM = GM.subClass({ imageMagick: true });
// TODO: change if sizes of cards and pages become parametrable
exports.xRepeat = 3;
exports.yRepeat = 3;
exports.CARDS_PER_PAGES = exports.xRepeat * exports.yRepeat;
exports.CARD_WIDTH = 635; // 3 * -> 1905
exports.CARD_HEIGHT = 880; // 3 * -> 2640
exports.PAGE_WIDTH = 2100;
exports.PAGE_HEIGHT = 2970;
exports.LEFT_BORDER = (exports.PAGE_WIDTH - 3 * exports.CARD_WIDTH) / 2;
exports.BOTTOM_BORDER = (exports.PAGE_HEIGHT - 3 * exports.CARD_HEIGHT) / 2;
const bufferIsJpeg = (b) => !!b && b.length > 2 && b[0] === 255 && b[1] === 216 && b[2] === 255;
const areBufferJpeg = R.reduce((acc, elem) => acc && bufferIsJpeg(elem), true);
const make1page = (images, dest, filename) => __awaiter(void 0, void 0, void 0, function* () {
    const page = IM(0, 0, '#fff').setFormat('jpeg'); // .resize(PAGE_HEIGHT, PAGE_WIDTH)
    if (images.length > exports.CARDS_PER_PAGES) {
        throw Error(`too many images (${images.length}) for one page`);
    }
    // images.map((n: string) => {
    //   IM(n).size((err, value) => {
    //     if (value.width !== CARD_WIDTH || value.height !== CARD_HEIGHT) {
    //       throw Error(`incorrect size for ${n}: ${value.width} / ${value.height} instead of ${CARD_WIDTH} / ${CARD_HEIGHT}`)
    //     }
    //   })
    // })
    images.map((v, i) => {
        const cardX = i % exports.xRepeat;
        const cardY = Math.floor(i / exports.yRepeat);
        const x = cardX * exports.CARD_WIDTH; // + LEFT_BORDER
        const y = cardY * exports.CARD_HEIGHT; // + BOTTOM_BORDER
        page
            // .in('-page', `${PAGE_WIDTH}x${PAGE_WIDTH}+${x}+${y}`) // for bordered this is the way, border value is missing from x/y
            .in('-page', `${exports.CARD_WIDTH}x${exports.CARD_HEIGHT} +${x}+${y}`)
            .in(v);
    });
    page.mosaic();
    // TODO: change ?
    const name = `${dest}${filename}.jpg`; // TODO: changer ça pour nom en argument + numéro de page
    page // .charcoal(.5) -> for low cost test print
        .in('-density 300 -units PixelsPerInch -resize 2480x3508 -trim')
        .write(name, () => ({}));
    return name;
});
/**
 * Group all the images in pages for print
 *
 * @param files string[] a list of images uri to be grouped
 * @returns a Promise with the uri of jpg created
 */
exports.files2print = (images, dest, filename) => __awaiter(void 0, void 0, void 0, function* () {
    images.map((f) => {
        if (f.slice(-4) !== '.jpg')
            throw Error(`${f} does not have a .jpg extension`);
        if (!fs_1.existsSync(f))
            throw Error(`${f} does not exists`);
    });
    const files = R.map(fs_1.readFileSync)(images);
    if (!areBufferJpeg(files))
        throw Error('A buffer is not a image/jpeg');
    // todo: check if files have the right size ?
    // TODO: check dest exists
    const cardsCount = images.length;
    const pagesCount = Math.ceil(cardsCount / exports.CARDS_PER_PAGES);
    const digits = pagesCount.toString().length;
    const promises = [...Array(pagesCount)].map((x, page) => {
        const first = Math.max(0, exports.CARDS_PER_PAGES * page);
        const last = Math.min(images.length, exports.CARDS_PER_PAGES * (page + 1));
        return make1page(images.slice(first, last), dest, `${filename}${(page + 1).toString().padStart(digits, '0')}`);
    });
    return Promise.all(promises);
});
//# sourceMappingURL=print.js.map
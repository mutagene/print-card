export declare const xRepeat = 3;
export declare const yRepeat = 3;
export declare const CARDS_PER_PAGES: number;
export declare const CARD_WIDTH = 635;
export declare const CARD_HEIGHT = 880;
export declare const PAGE_WIDTH = 2100;
export declare const PAGE_HEIGHT = 2970;
export declare const LEFT_BORDER: number;
export declare const BOTTOM_BORDER: number;
/**
 * Group all the images in pages for print
 *
 * @param files string[] a list of images uri to be grouped
 * @returns a Promise with the uri of jpg created
 */
export declare const files2print: (images: string[], dest: string, filename: string) => Promise<string[]>;

# Card print

Group cards graphics for easy print.

Can be feed with a list of cards, a list of lists (to separate decks, or recto/verso), only recto or recto/verso.

# Usage

# Requirements

[ImageMagick](https://imagemagick.org/index.php)



# Roadmap

Only jpg to jpg.

Only standard card size (63 x 88 mm / 2.49 x 3.48 in) will be allowed for now, and only A4 pages (210 x 297 mm) for rendering.
import chai from 'chai'
import chaiAsPromised from 'chai-as-promised'
chai.use(chaiAsPromised)
import { expect } from 'chai'
import * as fs from 'fs'
import * as path from 'path'
import * as tmp from 'tmp'
import { range } from 'ramda'

import { files2print } from '../src/print'

describe('Input validation', async () => {
  let tmpobj: tmp.DirResult,
    tempFolder: string

  // make a temp dir
  beforeEach(() => {
    tmpobj = tmp.dirSync()
    tempFolder = tmpobj.name + path.sep
  })

  // empty and remove the temp dir
  afterEach(() => {
    fs.readdirSync(tempFolder).map(f => fs.unlinkSync(tempFolder + f))
    tmpobj.removeCallback()
  })

  it('should throws an error on non .jpg files', async () => {
    const files1 = [tempFolder + 'test.jpg', tempFolder + 'test.png']
    files1.map(f => fs.writeFileSync(f, ''))
    await expect(files2print(files1, process.env.PWD + '/tmp/', 'test')).to.be.rejectedWith(/does not have a .jpg extension/)
  })

  it('should throws an error on non-existing files', async () => {
    const files1 = [tempFolder + 'test.jpg']
    await expect(files2print(files1, process.env.PWD + '/tmp/', 'test')).to.be.rejectedWith(/does not exists/)
  })

  it('should fail on non image/jpeg file', async () => {
    const file = tempFolder + 'test.jpg'
    fs.copyFileSync(process.env.PWD + '/tests/lenna.png', file)
    await expect(files2print([file], process.env.PWD + '/tmp/', 'test')).to.be.rejectedWith(/is not a image\/jpeg/)
  })

  it('1 picture make 1 page', async () => {
    const file = tempFolder + 'test.jpg'
    fs.copyFileSync(process.env.PWD + '/tests/lenna.jpg', file)
    const result = await files2print([file], process.env.PWD + '/tmp/', 'test')
    expect(result).to.have.length(1)
  })

  it('9 pictures make 1 page', async () => {
    const files: string[] = [...Array(9)].map((v, i) => `${tempFolder}test${i}.jpg`)

    files.map(file => fs.copyFileSync(process.env.PWD + '/tests/lenna.jpg', file))

    const result = await files2print(files, process.env.PWD + '/tmp/', 'test')
    expect(result).to.have.length(1)
  })

  it('10 pictures make 2 pages', async () => {
    const files: string[] = [...Array(10)].map((v, i) => `${tempFolder}test${i}.jpg`)

    files.map(file => fs.copyFileSync(process.env.PWD + '/tests/lenna.jpg', file))

    const result = await files2print(files, process.env.PWD + '/tmp/', 'test')
    expect(result).to.have.length(2)
  })

  it('works on mutagene', async () => {
    const files: string[] = range(1, 12).map(i => `${process.env.PWD}/tests/A${i}_recto.jpg`)

    // files.map(file => fs.copyFileSync(process.env.PWD + '/tests/lenna.jpg', file))

    const result = await files2print(files, process.env.PWD + '/tmp/', 'test')
    expect(result).to.have.length(2)
  })
})

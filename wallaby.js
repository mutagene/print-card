process.env.NODE_ENV = 'test';

module.exports = function (wallaby) {

  return {
    files: [
      'src/*.ts',
      'src/**/*.ts',
      'tsconfig.json',
      '!src/**/*.spec.ts',
    ],

    filesWithNoCoverageCalculated: [
      'src/*/*.interfaces.ts',
    ],

    tests: [
      'tests/**/*.spec.ts'
    ],

    env: {
      type: 'node',
      runner: 'node',
      // params: {
      //   env: 'NODE_ENV=test;'
      // }
    },

    testFramework: 'mocha',

    // setup: function (wallaby) {
    //   var mocha = wallaby.testFramework;
    //   mocha.timeout(20000)
    //   // TODO : utiliser une valeur fixe définie dans un .env ou autre
    // },

    workers: {  // à voir si nécessaire
      initial: 6,
      regular: 4,
      restart: true,
    }
    // TODO: ok pour wallaby, mais mocha lancé via npm ET ide : nok
    // setup: wallaby => {
    //   global.expect = require('chai').expect;
    // },
  };


}